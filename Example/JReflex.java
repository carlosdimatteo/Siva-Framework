package flex;

import java.lang.reflect.Method;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JReflex {
	private String objectName;
	private String methodName;
	private String[] typeParams;
	private Class<?>[] classParams;
	private Object[] values;
	
	public JReflex(String json) {
		parseParams(json);
	}
	
	public String getObjectName() {
		return objectName;
	}

	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public String[] getTypeParams() {
		return typeParams;
	}

	public void setTypeParams(String[] typeParams) {
		this.typeParams = typeParams;
	}
	
	private Class<?> getType_(String tp) {
		if(tp.equals("int")) {
			return int.class;
		}
		if(tp.equals("Integer")) {
			return Integer.class;
		}
		if(tp.equals("float")) {
			return float.class;
		}
		if(tp.equals("Float")) {
			return Float.class;
		}
		if(tp.equals("double")) {
			return double.class;
		}
		if(tp.equals("Double")) {
			return Double.class;
		}
		if(tp.equals("string")) {
			return String.class;
		}
		if(tp.equals("byte")) {
			return byte.class;
		}
		if(tp.equals("Byte")) {
			return Byte.class;
		}
		if(tp.equals("object")) {
			return Object.class;
		}
		if(tp.equals("char")) {
			return char.class;
		}
		if(tp.equals("Character")) {
			return Character.class;
		}
		if(tp.equals("boolean")) {
			return boolean.class;
		}
		if(tp.equals("Boolean")) {
			return Boolean.class;
		}
		if(tp.equals("ArrayList")) {
			return ArrayList.class;
		}
		return null;
	}
	
	private Object getValue_(String v, String tp) {
		if(tp.equals("int")) {
			return Integer.parseInt(v);
		}
		if(tp.equals("Integer")) {
			return new Integer(v);
		}
		if(tp.equals("float")) {
			return Float.parseFloat(v);
		}
		if(tp.equals("Float")) {
			return new Float(v);
		}
		if(tp.equals("double")) {
			return Double.parseDouble(v);
		}
		if(tp.equals("Double")) {
			return new Double(v);
		}
		if(tp.equals("string")) {
			return new String(v);
		}
		if(tp.equals("byte")) {
			return Byte.parseByte(v);
		}
		if(tp.equals("Byte")) {
			return new Byte(v);
		}
		if(tp.equals("char")) {
			return new Character(v.charAt(0));
		}
		if(tp.equals("Character")) {
			return new Character(v.charAt(0));
		}
		if(tp.equals("boolean")) {
			return Boolean.parseBoolean(v);
		}
		if(tp.equals("Boolean")) {
			return new Boolean(v);
		}
		if(tp.equals("ArrayList")) {
			ArrayList<Object> al = new ArrayList<Object>();
			try {
				JSONParser parser = new JSONParser();
				JSONArray p = (JSONArray) parser.parse(v);
				for(int i=0; i<p.size(); i++)
					al.add(p.get(i));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return al;
		}
		return null;
	}	
	
	private void parseParams(String json) {
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(json);
			JSONObject jo = (JSONObject) obj;
			objectName = (String) jo.get("objectName");
			methodName = (String) jo.get("methodName");
			JSONArray p = (JSONArray) parser.parse(jo.get("params").toString());
			JSONArray tp = (JSONArray) parser.parse(jo.get("typeParams").toString());
			classParams = new Class[p.size()];
			values = new Object[p.size()];
			for(int i=0; i<p.size(); i++) {
				String par = tp.get(i).toString();
				values[i] = getValue_(p.get(i).toString().trim(), par);
				classParams[i] = getType_(par.trim());
			}	
		} 
		catch (org.json.simple.parser.ParseException e) {
			e.printStackTrace();
		}
	}
	
	public Object exeMethod() {
		try {
			Class<?> clase = Class.forName(objectName);
			Object obj = clase.newInstance();
			Method method = clase.getDeclaredMethod(methodName, classParams);
			return method.invoke(obj, values);
		} 
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		finally {
			objectName = null;
			methodName = null;
			classParams = null;
			values = null;
		}
	}
	
	public Object exeMethod(String json) {
		parseParams(json);
		return exeMethod();
	}
}