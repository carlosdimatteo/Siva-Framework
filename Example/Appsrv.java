package flex;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import flex.JReflex;

public class Appsrv {	
	private String contentType = "text/html";
	private String s = "{\"objectName\" : \"jxr.MyObject\",\"methodName\" : \"myMethod3\",\"params\" : [\"[1,\\\"hola2\\\",3.5]\",true],\"typeParams\": [\"ArrayList\",\"boolean\"]}";		
	
	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	
	private boolean autenticar() {
		return true;
    }
	
	private Boolean checkPermission(String ticket) {
		return true;
	}
	
	private void sendToClient(String response, HttpServletResponse resp) {
		try {
			resp.setContentType(getContentType()); 
			Writer w;
			w = resp.getWriter();
			w.write(response);
	        w.flush();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private String executeMethod(String json) {
		try{            
            JReflex jr = new JReflex(json);  
            return jr.exeMethod().toString();         
	    }
		catch(Exception pe){
        	System.out.println("position: " + pe.getMessage());
        	System.out.println(pe);
        	return null;
		}	
	}
	
	@SuppressWarnings({ "serial", "deprecation" })
	public static void main(String[] args) throws LifecycleException, InterruptedException, ServletException{
		Appsrv as = new Appsrv();
		Tomcat tomcat = new Tomcat();
	    tomcat.setPort(8080);
	    Context ctx = tomcat.addContext("/", new File(".").getAbsolutePath());   
	    Tomcat.addServlet(ctx, "hello", new HttpServlet() {
	      protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
	    	doPost(req, resp);	    	
	      }
	      
	      protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
	    	  HttpSession session=req.getSession();
	    	  if(session.isNew()) {	    		  
	    		  if(as.autenticar()) {
	    			  session.setAttribute("ticket", "123");
	    			  as.sendToClient("{\"sts\": true, \"msg:\"'Acceso garantizado..!'}", resp);
	    		  }
	    		  else {
	    			  as.sendToClient("{\"sts\": false, \"msg\":'Acceso denegado..!'}", resp);
	    		  }
	    	  }
	    	  else {
	    		  String att = (String) session.getAttribute("ticket");	    		  
	    	  }
		  }
	      
	    });	    	    
	    ctx.addServletMapping("/*", "hello");
	    tomcat.start();
	    tomcat.getServer().await();
	}
}
