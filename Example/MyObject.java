package flex;

import java.util.ArrayList;

public class MyObject {
	public String myMethod(String s) {
		System.out.println("se recibi� : "+s);
		return "se retorna : ok";
	}
	public String myMethod2(String s, boolean x) {
		System.out.println("se recibi� : "+s+"  "+x);
		return "se retorna : ok";
	}
	public String myMethod3(ArrayList<?> s, boolean x) {
		System.out.print("[");
		for(int i=0; i<s.size(); i++) {
			if(i<s.size()-1)
				System.out.print(s.get(i)+",");
			else
				System.out.print(s.get(i));
		}
		System.out.println("]");
		return "{\"sts\":true, \"msg\":'ok'}";
	}
}
