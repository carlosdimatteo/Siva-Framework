package flex;

public class Ticket {
	String userName;
	int idPerfil;
	boolean sts;
	
	public boolean isSts() {
		return sts;
	}
	public void setSts(boolean sts) {
		this.sts = sts;
	}
	public Ticket(String userNm, int idPerf){
		this.userName=userNm;
		this.idPerfil = idPerf; 
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public int getId_perfil() {
		return idPerfil;
	}
	public void setId_perfil(int id_perfil) {
		this.idPerfil = id_perfil;
	}
}
