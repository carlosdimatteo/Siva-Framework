const username = new SivaInput();
username.id = "user";
username.innerText = 'Username';

const password = new SivaInput();
password.id = "password";
password.innerText = 'Password';

const BOTest = new SivaButton();
BOTest.id = 'BOTest';
BOTest.innerText = 'Siva Validate';
BOTest.method = 'BO_Test'

const BOCheck = new SivaButton();
BOCheck.id = 'BOCheck';
BOCheck.innerText = 'Siva Check';
BOCheck.method = 'BO_Check'

const BODate = new SivaButton();
BODate.id = 'BODate';
BODate.innerText = 'Siva Date';
BODate.method = 'BO_Date'

document.body.appendChild(username);
document.body.appendChild(password);
document.body.appendChild(BOTest);
document.body.appendChild(BOCheck);
document.body.appendChild(BODate);