class SivaInput extends HTMLElement {
    constructor() {
        super();
    }

    connectedCallback() {
        this.innerHTML = ` ${this.innerText}: <input id=${this.id}-comp></input>`
    }
}

customElements.define("siva-comp-input", SivaInput);