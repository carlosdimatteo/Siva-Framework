const bobjects = 'siva.bobjects.';

const user = document.querySelector('#user-comp');
const pass = document.querySelector('#password-comp');

BO_Test = () => {
    let arr = [user.value, pass.value]
    let typeParams = ["string", "string"]

    const options = {
        username: user.value,
        password: pass.value,
        objName: `${bobjects}SessionObject`,
        metName: 'Validate',
        params: arr,
        typeParams: typeParams
    }

    fetching(options, 'POST', './Siva', response => {
        console.log(response)
    })
}

BO_Check = () => {
    let arr = [user.value, true]
    let typeParams = ["string", "boolean"]

    const options = {
        username: user.value,
        password: pass.value,
        objName: `${bobjects}SecurityObject`,
        metName: "Checking",
        params: arr,
        typeParams: typeParams
    }

    fetching(options, 'POST', './Siva', response => {
        console.log(response)
    })
}

BO_Date = () => {

    const options = {
        objName: `${bobjects}BussinessObjects`,
        metName: 'Date',
        params: "[]",
        typeParams: "[]"
    }

    fetching(options, 'POST', './Siva', response => {
        console.log(response)
    })
}