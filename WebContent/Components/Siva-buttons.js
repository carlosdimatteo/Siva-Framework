class SivaButton extends HTMLElement {
    constructor() {
        super();
        this.method = this.method;
    }

    connectedCallback() {
        this.innerHTML = ` <button id=${this.id} onClick=${this.method}()>${this.innerText}</button>`;
    }
}

customElements.define("siva-comp-button", SivaButton);