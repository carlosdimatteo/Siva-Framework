# Siva-Framework

Java Framework

## Getting Started

```
git clone https://github.com/eduardogz94/Siva-Framework.git
```

## Built With

* [postgresql-9.3-1102.jdbc41.jar](https://) - JDBC 
* [](https://) - DESCRIPTION
* [](https://) - DESCRIPTION

## Authors

* **Eduardo Gonzalez** - *Initial work* - [Freelance](https://github.com/eduardogz94)
* **Jean Lambert** - *Initial work* - [Freelance](https://github.com/Jnlbr)
* **Giovanny Santos** - *Initial work* - [Freelance](https://github.com/gssantost)
* **Carlos Di Matteo** - *Initial work* - [Freelance](https://github.com/carlosdimatteo)
* **Cesar Brazon** - *Initial work* - [Freelance](https://github.com/cbrzn)
* **Jose Mundo** - *Initial work* - [Freelance](https://github.com/JOENMUPI)
* **Daniel Marquez** - *Initial work* - [Freelance](https://github.com/DaniielM)
* **Daniel Reverol** - *Initial work* - [Freelance](https://github.com/danchini)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration
* etc

