package siva.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import siva.utilities.Encrypt;
import siva.utilities.PropertiesReader;
import siva.utilities.SingleDB;

public class Database {
	public ResultSet rs;
	private PreparedStatement pstmt;
	private Connection con;
	private PropertiesReader prop = PropertiesReader.getInstance();
	private Encrypt md;
	
	public Database(){
		try {
			SingleDB db = SingleDB.getInstance();
			Class.forName(db.getDriver());
			this.con= DriverManager.getConnection(db.getUrl(), db.getUsername(), db.getPassword());
		}
		catch(Exception e){
			e.getStackTrace();
		}
	}
	
	public boolean checkUser(String email, String password) {
		boolean state = false;
		try {
			this.pstmt = con.prepareStatement(prop.getValue("query_checkuser"));
			this.pstmt.setString(1, email);
			this.pstmt.setString(2, password);
			this.rs = pstmt.executeQuery();
			state = this.rs.next();
			} catch (Exception e) {
				e.getStackTrace();
			}
		return state;
	}
	
	public boolean checkAdmin(String email) {
		boolean state = false;
		try {
			this.pstmt = con.prepareStatement(prop.getValue("query_admin"));
			this.pstmt.setString(1, email);
			this.rs = this.pstmt.executeQuery();
			rs.next();
			if(this.rs.getString("type_id").equals("1")) {
				state = true;
			}
		} catch(Exception e) {
			e.getStackTrace();
		}
		return state;
	}
	
	public boolean checkSign(String email) {
		boolean state = false;
		try {
			this.pstmt = con.prepareStatement(prop.getValue("query_checkusersign"));
			this.pstmt.setString(1, email);
			this.rs = this.pstmt.executeQuery();
			boolean state1 = this.rs.next();
			if(state1) {
				System.out.println("Email exists");
			} else{
				state = true;
				System.out.println("Sign up completed");
			}
		} catch (Exception e) {
			e.getStackTrace();
		}
		return state;
	}
	
	public void newAccount(String username, String password) {
		try {
			md = new Encrypt(password);
			this.pstmt = con.prepareStatement(prop.getValue("query_new"));
			this.pstmt.setString(1,username);
			this.pstmt.setString(2,md.returnEncrypt());
			this.pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public int userId(String email) {
		int id = 0;
		try {
			this.pstmt = this.con.prepareStatement(prop.getValue("query_getId"));
		    this.pstmt.setString(1, email);
			this.rs = this.pstmt.executeQuery();
				while (this.rs.next()) 		
					id = this.rs.getInt("id_user");
				return id;
			} catch (Exception e) {
				e.printStackTrace();
			}
		return id;
	}
	
	public void closeCon() {
		try {
			this.con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
