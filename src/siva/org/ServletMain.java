package siva.org;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import siva.controller.Controller;

@WebServlet("/ServletMain")
public class ServletMain extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public static Controller exec = new Controller();
	
	public ServletMain() {
        super();
    }
	
	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
        	HttpSession session = request.getSession();
        	String ticket = (String) session.getAttribute("ticket");
        	
        	JSONParser parser = new JSONParser();
        	Object req = parser.parse(request.getReader());
        	JSONObject reqBody = (JSONObject) req;
			
			String username = reqBody.get("username").toString();
			String pass = reqBody.get("password").toString();
			
			JSONObject res = new JSONObject();
			
			System.out.println(reqBody);
			System.out.println("sesion? " + ticket);
			
			if (session.isNew()) {
				if (exec.authenticate(username,pass)) {
					Object msg = exec.executeMethod(reqBody.toString());

					res.put("logged?", true);
					res.put("Acess?", "Access granted");
					res.put("Response", msg);
					session.setAttribute("sucess", msg);
					
					storeValue(username, msg ,session);
				} else {
					res.put("logged?", false);
					res.put("Acess?", "Acess denied");
					session.invalidate();
				}
			} else {
				JSONArray ses = new JSONArray();
				
				ses.add(ticket);
				ses.add(true);
				
				JSONObject logged = new JSONObject();
				
				logged.put("objName", "siva.bobjects.SessionObject");
				logged.put("metName", "LoggedIn");
				logged.put("params", ses);
				logged.put("typeParams", "[\"string\", \"boolean\"]");
				
				Object msg = exec.executeMethod(logged.toString());
				res.put("Response", msg);
			}
			exec.sendToClient(res.toString(), response);
        } catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	private void storeValue(String ticket,Object msg, HttpSession session) {
			session.setAttribute("ticket", ticket);
			session.setAttribute("logged?", true);
			session.setAttribute("sucess", msg);
	}
}