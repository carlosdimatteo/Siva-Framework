package siva.org;

import javax.servlet.ServletException;

import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;


public class Main {
	static ServletMain servlet = new ServletMain();
	
	public static void main(String[] args) throws LifecycleException {
		try {
			Integer port = 3000;
			Tomcat tomcat = new Tomcat();
			Context ctxt = null;	
			
		    tomcat.setPort(port);
		    
			String web_app = "WebContent";
			ctxt = tomcat.addWebapp("/", System.getProperty("user.dir") + "/" +web_app);
			
			Tomcat.addServlet(ctxt, "Controller", servlet);
			ctxt.addServletMappingDecoded("/Siva", "Controller");
			
			tomcat.start();
			System.out.println("Siva Framework Waiting for request");
			tomcat.getServer().await();
			
		} catch (ServletException e) {
			e.printStackTrace();
		}
	}
}
