package siva.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import siva.db.Database;


public class Controller {
	@SuppressWarnings("rawtypes")
	Class process,db,security;
	Object client;
	
	public Controller() { 
		try {
			process = Class.forName("siva.controller.ProcessParams");
			db = Class.forName("siva.db.Database");
//			client = clase.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String contentType = "application/json";
	
	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	
	public boolean authenticate(String name, String pass) {
		Database db = new Database();
		if(db.checkUser(name, pass)) {
			return true;
		} else {
			return false;
		}
    }
	
	public void Exec(String json){
		executeMethod(json);
	}
	
	public Boolean checkPermission(String ticket) {
		return true;
	}
	
	public void sendToClient(String response, HttpServletResponse resp) {
		try {
			resp.setContentType(getContentType()); 
			PrintWriter w;
			w = resp.getWriter();
			w.write(response);
	        w.flush();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Object executeMethod(String json) {
		try{            
            ProcessParams client = new ProcessParams(json);  
//			clase.exeMethod();
            return client.exeMethod().toString();         
	    }
		catch(Exception pe){
        	System.out.println("position: " + pe.getMessage());
//        	System.out.println(pe);
        	return null;
		}	
	}
}