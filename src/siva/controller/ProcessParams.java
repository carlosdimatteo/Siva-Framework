package siva.controller;

import java.lang.reflect.Method;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ProcessParams {
	private String objectName;
	private String methodName;
	private String[] typeParams;
	private Class<?>[] classParams;
	private Object[] params;
	
	public ProcessParams(String json) {
		parseParams(json);
	}
	
	public String getObjectName() {
		return objectName;
	}

	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public String[] getTypeParams() {
		return typeParams;
	}

	public void setTypeParams(String[] typeParams) {
		this.typeParams = typeParams;
	}
	
	private Class<?> getType(String paramType) {
		switch (paramType) {
		case "int":
			return int.class;
		case "Integer":
			return Integer.class;
		case "float":
			return float.class;
		case "Float":
			return Float.class;
		case "double":
			return double.class;
		case "Double":
			return Double.class;
		case "string":
			return String.class;
		case "byte":
			return byte.class;
		case "Byte":
			return Byte.class;
		case "object":
			return Object.class;
		case "char":
			return char.class;
		case "Character":
			return Character.class;
		case "boolean":
			return boolean.class;
		case "Boolean":
			return Boolean.class;
		case "ArrayList":
			return ArrayList.class;
		default:
			return null;
	
		}
	}
	
	private Object getParams(String param, String type) {
		switch (type) {
			case "int":
				return Integer.parseInt(param);
			case "Integer":
				return new Integer(param);
			case "float":
				return Float.parseFloat(param);
			case "Float":
				return new Float(param);
			case "double":
				return Double.parseDouble(param);
			case "Double":
				return new Double(param);
			case "string":
				return new String(param);
			case "byte":
				return Byte.parseByte(param);
			case "Byte":
				return new Byte(param);
			case "char":
				return new Character(param.charAt(0));
			case "Character":
				return new Character(param.charAt(0));
			case "boolean":
				return Boolean.parseBoolean(param);
			case "Boolean":
				return new Boolean(param);
			case "ArrayList":
				ArrayList<Object> al = new ArrayList<Object>();
				try {
					JSONParser parser = new JSONParser();
					JSONArray p = (JSONArray) parser.parse(param);
					for(int i=0; i<p.size(); i++)
						al.add(p.get(i));
					} catch (ParseException e) {
						e.printStackTrace();
					}
					return al;
			default:
				return null;
		}
	}	
	
	private void parseParams(String json) {
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(json);
			JSONObject jo = (JSONObject) obj;
			objectName = (String) jo.get("objName");
			methodName = (String) jo.get("metName");
			JSONArray parameters = (JSONArray) parser.parse(jo.get("params").toString());
			JSONArray parameterTypes = (JSONArray) parser.parse(jo.get("typeParams").toString());
			classParams = new Class[parameters.size()];
			params = new Object[parameters.size()];
			for(int i=0; i<parameters.size(); i++) {
				String param = parameterTypes.get(i).toString();
				params[i] = getParams(parameters.get(i).toString().trim(), param);
				classParams[i] = getType(param.trim());
			}	
		} 
		catch (org.json.simple.parser.ParseException e) {
			e.printStackTrace();
		}
	}
	
	public Object exeMethod() {
		try {
			Class<?> clase = Class.forName(objectName);
			Object obj = clase.newInstance();
			Method method = clase.getDeclaredMethod(methodName, classParams);
			return method.invoke(obj, params);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			objectName = null;
			methodName = null;
			classParams = null;
			params = null;
		}
	}
	
	public Object exeMethod(String json) {
		parseParams(json);
		return exeMethod();
	}
}